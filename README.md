### React Items List
![image](https://gitlab.com/tyler.johnson/react-items-list/raw/master/example/react-items-list.png "React Items List")

This is a todo style list that allows creation of new items as well as removal and editing of existing items.

There is a live example [here](http://tyler.johnson.gitlab.io/react-items-list/app.html "example")

An *items* array prop can be provided to prepopulate the list.

Callbacks named *onAdd*, *onUpdate*, *onRemove* can be provided as props.

 - onAdd will be called with the text that was added.
 - onUpdate will be called with the index and new value.
 - onRemove will be called with the affected index.

See the [example](https://gitlab.com/tyler.johnson/react-items-list/blob/master/example/app.js) for sample use.

## Development
```bash
npm install react-items-list
npm run setup-test-env #expects google-chrome-stable in path
npm run test
```
