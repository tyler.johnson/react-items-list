#!/bin/env bash
node_modules/.bin/jshint components test && node_modules/.bin/mocha test && node_modules/.bin/browserify example/app.js --fast -o example/app-browserified.js && (curl -s localhost:3456 || :)
