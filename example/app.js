let dom = require("react-dom"),
react = require("react"),
listClass = require("../components/list.js"),
items = ["test 1", "test 2", "test 3"];

const app = ()=>{
  return react.createElement(listClass, {
    items: items,
    onAdd(text) {
      console.log("added " + text);
    },
    onUpdate(idx, text) {
      console.log(`updated index ${idx} to ${text}`);
    },
    onRemove(idx) {
      console.log("removed index " + idx);
    }
  });
};

dom.render(react.createElement(app), document.getElementById("app"));
