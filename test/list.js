const assert = require("assert"),
react = require("react"),
listClass = require("../components/list.js"),
testUtils = require("react-addons-test-utils");

global.navigator = {userAgent: "test"};

describe("List Test", ()=>{
  let renderer = testUtils.createRenderer(),
  list;

  function rows() {
    return list.props.children.props.children;
  }

  function row(zeroIndexRowNum) {
    return {
      fieldType: rows()[zeroIndexRowNum].props.children[0].type.name,
      iconType: rows()[zeroIndexRowNum].props.children[1].type.name,
      clickButton() {
        rows()[zeroIndexRowNum].props.children[1].props.onClick();
      },
      enterText() {
        rows()[zeroIndexRowNum].props.children[0].props.onChange({
          currentTarget: {
            value: "test"
          }
        });
      },
      pressEnter() {
        rows()[zeroIndexRowNum].props.children[0].props.onKeyUp({
          key: "Enter"
        });
      }
    };
  }

  beforeEach(()=>{
    renderer.render(react.createElement(listClass, {items: [1, 2, 3]}));
    list = renderer.getRenderOutput();
  });

  it("exists as a react component", ()=>{
    assert.ok(testUtils.isElement(list));
  });

  it("renders multiple list items with TextField and IconButton", ()=>{
    assert.equal(list.props.children.props.children.length, 4);
    assert.equal(row(0).fieldType, "TextField");
    assert.equal(row(0).iconType, "IconButton");
  });

  it("accepts and calls add, update, remove handlers", ()=>{
    let added = 0, updated = 0, removed = 0;

    function onAdd(text) {
      added += 1;
    }

    function onUpdate(text) {
      updated += 1;
    }

    function onRemove(text) {
      removed += 1;
    }

    renderer = testUtils.createRenderer();
    renderer.render(react.createElement(listClass, {
      items: [1, 2, 3], onAdd, onUpdate, onRemove
    }));

    list = renderer.getRenderOutput();
    row(3).enterText("test");
    row(3).clickButton();
    row(0).enterText("test");
    row(0).pressEnter();
    row(1).clickButton();

    assert.equal(added, 1);
    assert.equal(updated, 1);
    assert.equal(removed, 1);
  });
});
